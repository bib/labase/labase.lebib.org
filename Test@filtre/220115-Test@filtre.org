15 janvier 2022

* Présent-e-s
Imago, Géronimo , Tangui, Calam

* Manips
Aujourd'hui nous faisons des réplicats techniques. Nous reproduirons ces experiences trois fois pour optenir une variance. Ces prochaines expériences correspondront à des réplicats biologiques.

Nous testons :
- neg neutre (sans bactérie)
- neg masque chirurgical
- filtre à aspirateur marque wonderbag universal original
- sopalin okay décor
- sopalin apta classique compact
- masque de l'IRD non homologué distribué au début de la crise
- lingette swiffer poussière dry
- contrôle positif

* Remarques

- Nébulisation d'alcool à 70° dans le système avant toute manipulation.

- Pour chaque 2 min de nébulisation 1 ml de solution bactériologique traverse le système.

- 100 µL de solution récupérée après test@filtrage et déposée dans la boîte de petri

- Nouveau point du protocole : Avant chaque nouvelle manipulation ajouter 10 gouttes d'une solution à même concentration bactérienne (ou équivalent de la solution bactériologique nébulisée durant la manip précédente) à savoir solution de /Echerichia coli/ K12 MG1655 à 100µl

- Le controle positif a été fait sur 3 boites de pétri :
    1. dilué à 10^{-2} ;
    2. dilué à 10^{-1} ;
    3. à aucune dilution ;

