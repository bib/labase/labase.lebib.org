# Publication

Espace de publication en ligne des projets de LABASE

## Pour créer un onglet et sa page correspondante
+ Aller dans "config.toml"

faire un copier coller d'un onglet en changeant les titres

+ Aller dans le dossier "content" 
- créer un fichier avec le titre de la page.md

## Pour documenter un atelier
+ aller dans le dossier "content" 
- écrire en markdown

## Pour écrire en markdown
https://learn.netlify.app/fr/cont/markdown/

## lien du site web

https://bib.frama.io/labase/labase.lebib.org
