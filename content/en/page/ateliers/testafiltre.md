---
title: DIY Test@Filtre
subtitle: Initiative de recherche-action afin de mettre en place un dispositif autonome (DIY) permettant d'effectuer des tests de l'efficacité de filtration des masques fait-maison en période d'épidémie.
date: 2021-11-04T18:49:12+01:00
comments: false
---

### DIY Test@Filtre

Présentation, explication, c'est quoi ? Où ? Qui ? Quand ? Enjeux ?


- [Déroulement de l'initiative](../testafiltre/deroulement/)
- [Guide de reproductibilité](../testafiltre/guide/)
- [Les articles](../testafiltre/articles/)
- [Les résultats](../testafiltre/resultats/)
