---
title: A propos
subtitle: LABASE
comments: false
---

## Le biohackerspace
LABASE est un lieu d’expérimentation en biologie où l’on peut faire ensemble ce qu’on ne peut pas faire ailleurs, c’est à dire dans les labos institutionnels (publics ou privés) avec des objectifs de création, d’autonomie, d'auto-production et d’émancipation face aux dominations que chacun-e subit.

Les valeurs centrales défendues par LABASE sont nombreuses : expérimentation, création, autonomie (sanitaire, alimentaire et énergétique), auto-production et émancipation face aux dominations (sociales, économiques, ethniques, de genre, etc), activité ludique, co-formation, partages non-commerciaux, low-tech, alternatif, solidarité, animations, échanges entre celleux qui savent et de celleux qui veulent agir (lutte et mouvement), production physique en plus de la production de savoir (du concret en plus de la connaissance), collecte et transmission libre des données.

L'organisation de LABASE se veut la plus horizontale possible, avec les discussions et les prises de décisions en réunion, et notamment les validations lors de la réunion mensuelle (voir ci-dessous) des différents événements/ateliers qui y sont réalisés. LABASE n'est donc pas une pépinière d'entreprise où chacun-e crée, expérimente, produit dans son coin et pour son compte, mais bien un lieu d'expérimentation collective (et le collectif, ça commence à partir de deux ! donc rien n'est perdu !).
