---
title: Auto-gynécologie
subtitle: Diagnostic, analyse et expérimentation de traitement en phytothérapie
comments: false
---

### Atelier "auto-gynéco"

Les femmes, guérisseuses ou sorcières ont organisé les savoirs relatifs à la santé gynécologique pendant des siècles. Ces savoirs ont ensuite été confisqués par les hommes, suite à la professionnalisation de la médecine et la relégation des femmes dans l’espace domestique, notamment pour permettre un contrôle de la reproduction.

Les pratiques d’auto-médication relatives à la santé vaginale n’ont pourtant pas cessé de perdurer depuis des générations ! On trouve aujourd’hui un certain nombre de témoignages oraux et/ou publiés (papiers ou internet), comme par exemple l’application de yaourt dans les cas de mycose ou vaginose ou d’huiles essentielles. Malheureusement, peu d’informations sont disponibles quant aux protocoles précis de fabrication, de concentration de principes actifs, de posologie, de mode d’applications et de risques sanitaires de ces pratiques.

### Objectifs de l'atelier

L’atelier « auto-gynéco » se propose, en se rencontrant régulièrement, de répertorier et documenter les pratiques existantes de manières précises. De manière expérimentale, nous pourrions aussi tester l’efficacité in vitro des solutions sur certaines bactéries non pathogènes (par ex. Lactobacillus) ainsi que d’envisager les modalités d’une auto-production de bactéries probiotiques et autres solutions.

Il s’agira enfin, lors de l’atelier, d’analyser comment le « do-it-yourself-bio » peut constituer, ou non, une forme de réappropriation de ces savoirs, et quelles formes peut prendre, dans ce cas précis, l’articulation entre production des savoirs et luttes féministes.
