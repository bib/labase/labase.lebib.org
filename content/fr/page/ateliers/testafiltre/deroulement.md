---
title: Déroulement de l'initiative
subtitle: Suivi chronologique et explications
comments: false
---

# Table of Contents

1.  [Chronologie de l'activité de LABASE durant les événements covidiens](#orgbbbd81e)
    1.  [Période de confinement (17 mars au 11 mai)](#orgdb74e84)
        1.  [23 mars](#org94a1c45)
        2.  [25 mars](#orge505fec)
        3.  [26 mars](#orgf5c7757)
        4.  [27 mars](#org48b92c0)
        5.  [28 mars](#orgbca7bd4)
        6.  [30 mars](#org218a534)
        7.  [2 avril](#org15994cc)
        8.  [3 avril](#orgb8eebdc)
        9.  [6 avril](#orgd34a2f8)
        10. [8 avril](#org582e219)
        11. [10 avril](#orgf0b9cb7)
        12. [Courant avril](#orgf61faf6)
        13. [1er mai](#org69a9d83)
        14. [4 mai](#org54d8774)
    2.  [période de déconfinement  (11 mai au 30 octobre)](#orgbf4c377)
        1.  [11 et 12 mai](#org5f5a468)
        2.  [13 mai](#orgaa590a2)
        3.  [14 mai](#org7d98c62)
        4.  [18 mai](#org083874d)
        5.  [du 19 mai au 29 juillet](#orgb7468c9)
    3.  [Courant Août](#org366548a)
    4.  [Courant Septembre](#org24551b8)
    5.  [Début octobre](#orgcf9ca0e)
    6.  [Période de reconfinement (30 octobre au &#x2026;)](#org05296e4)
        1.  [mi- et fin-octobre (reconfinement)](#orga313ba3)


<a id="orgbbbd81e"></a>

# Chronologie de l'activité de LABASE durant les événements covidiens

*Personnes actives :*
Géronimo, Jeanne, Lucas, Erwan, Pierre, Maya, Françoise, Lucile, Gilles, Jennifer, Gauthier, Tangui


<a id="orgdb74e84"></a>

## Période de confinement (17 mars au 11 mai)

Le collectif LABASE se réunit en quotidienne d'une durée d'environ 1h à 3h (à 14h sur la [plateforme Jitsi](https://meet.jit.si/BIB-LABASE-COVID19)). Au bout de 2 semaines nous avons décidé naturellement de mettre en pause la quotidienne le week-end. L'idée au démarrage est de mettre en place un espace d'entraide, de compréhension du COVID-19 et d'actions collectives ; De se poser collectivement les questions : comment réagir ? Que faire ? Comment être utile ?

*Remarque introspective :*
Les échanges d'informations, la collaboration sur des actions collectives ont rythmé nos journées confinées.


<a id="org94a1c45"></a>

### 23 mars

-   Premier jour de la quotidienne : on décide de se retrouver tous les jours à 14H
-   debut du dokuwiki pour la documentation -> outils utilisés  : gitlab (developpeur), wiki (libriste), zotero (chercheur), radio libre (cracker?)


<a id="orge505fec"></a>

### 25 mars

-   1ère évocation d'un test de l'efficacité de filtration des masques.
-   Géro documente les 3 premiers jours sur le dokuwiki puis créer un accès à Tangui, qui prend le relais.


<a id="orgf5c7757"></a>

### 26 mars

-   Recherche d'articles et d'informations sur l'épidémie et le virus du COVID-19
-   Réflexion sur nos modes d'actions (réalisation de masques DIY + fabrication de gel hydro-alcoolique + ce que peut faire un biohackerspace) ainsi que recherche d'acteurs, collectifs, groupes actifs sur Montpellier pour s'auto-organiser en réseau d'entraide local.
-   Début de l'initiative avec CoVie Entraide (se greffer à un collectif déjà dynamique pour impulser la dynamique de LABASE, mobiliser un maximum d'acteurs) pour fédérer, organiser, produire des outils collectifs et agir auprès des plus vulnérables (démarche solidaire)


<a id="org48b92c0"></a>

### 27 mars

-   Masque DIY créé par Tangui
-   LEPIED (association de paysan-cuisinier) peut donner des legumes
-   Réalisation d'un kit aidé et kit aidant pour réseau d'entraide local.


<a id="orgbca7bd4"></a>

### 28 mars

-   Partage d'infos fiables sur le COVID-19 et discussions sur la situation, le contexte actuel.
-   Nous implémentons une [page sur le COVID-19](https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19) pour rassembler les références, les sources, les inspirations, les initiatives et les informations covidiennes.


<a id="org218a534"></a>

### 30 mars

-   2ème évocation du test
-   Mise en place du [Zotero](https://www.zotero.org/groups/2462558/bib/library)
-   Début du détachement progressif de CoVie Entraide après avoir structuré et mis en route les initiatives avec le collectif pour se focaliser sur les actions possibles du biohackerspace.


<a id="org15994cc"></a>

### 2 avril

-   Notre choix s'est arrêté sur le projet (atelier) **DIY Test@Filtre** *expérimentations, protocoles, prototype, tests, documentation, résultats scientifiques, transmission et publication*. Notamment déclenché suite à une invitation à publier pour la [Voix du jaguar](https://lavoiedujaguar.net/). Notons la présence de Lucas, Géronimo, Tangui et Jeanne, qui sont des chercheurs. La perspective de la publication et de trouver un moyen de répondre à des besoins de sociétés à motiver la concentration sur un projet propre à LABASE
-   Nous parlons de [Afnor](https://www.afnor.org/actualites/coronavirus-telechargez-le-modele-de-masque-barriere/) pour la première fois
-   Début des questionnements sur la conception, l'ingénierie de projet.


<a id="orgb8eebdc"></a>

### 3 avril

-   Nous entrons dans une partie technique (effet venturi, nebulisation, boîte à toux, aérosol, bactériophage, analyses microbiologiques, bactéries pour simuler le virus).
-   Le processus des chaînes de coopération est lancé (process de création-production-recherche collective)


<a id="orgd34a2f8"></a>

### 6 avril

-   Nous commençons à réaliser des schémas au travers de la visio pour expliquer nos idées, inventions, bricolages. Puis réalisons fréquement un schéma récapitulatif (évolutif) de notre dispositif DIY schéma.
-   Le <https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings>[FramaGit]] démarre, notamment pour effectuer une documentation plus complète.


<a id="org582e219"></a>

### 8 avril

-   Nous commençons à prototyper le dispositif DIY Test@Filtre que nous appelons alors "bouttière" puisque nous utilisons des matériaux de récupération, de recyclage ou très accessibles parce que communs pour le fabriquer (bouteille en plastique, éléments d'une cafetière italienne) -> [Schéma](https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings/Sch%C3%A9ma%20Syst%C3%A8me%20de%20Test%20Masque%20DIY).


<a id="orgf0b9cb7"></a>

### 10 avril

-   Nous explorons la possibilité d'utiliser autre chose que des bactéries afin que ce soit plus accessible et reproductible. L'Atelier Métissé (atelier textile collaboratif à Avignon) a mis en place 3 tests empiriques pour connaître l'efficacité filtrante de leurs masques (durant la crise, ils ont produits jusqu'à 10 000 masques par semaine pour les collectivités, entreprises en associant une machine de découpe semi-industrielle et un réseau de couturier), notamment grâce à la granulométrie. Les masques avaient 3 couches et étaient fait en textile végétal
-   Tests empiriques :
-   Bougie s'eteind (DENSITÉ)
-   Fer à repasser, pigment coloré dans la vapeur d'eau qui se retrouve de l'autre coté du masque (AEROSOLÉ)
-   Spray de 3,5 bar (simulation toux), pigment qui traverse (PRESSION)


<a id="orgf61faf6"></a>

### Courant avril

-   Ingénierie de projet à distance : matériaux, choix, structure du dispositif, low-tech, DIY, écriture des protocoles, prototypages et expérimentations, visualisations. *(Annexe PDF compte-rendu Tangui : images, traces, captures écran, vidéos)*. [<https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:biblio-masks>][Réalisation d'une bibliographie relative au Sars-cov-2]].


<a id="org69a9d83"></a>

### 1er mai

-   Lancement d'une [page de financement participatif](https://www.helloasso.com/associations/le-bib-hackerspace/collectes/diy-test-filtre) (crowdfunding) pour l'atelier DIY Test@Filtre.


<a id="org54d8774"></a>

### 4 mai

-   Récupération d'une culture de bactérie par Géronimo : *Escherichia coli*


<a id="orgbf4c377"></a>

## période de déconfinement  (11 mai au 30 octobre)

Le confinement étant fini, la quotidienne COVID-19 de LABASE se retrouve en chair et en os (masqué) pour commencer les manipulations dans le laboratoire (BIB - La Tendresse).


<a id="org5f5a468"></a>

### 11 et 12 mai

-   Mise en place de l'atelier DIY Test@Filtre -> montage du premier prototype/dispositif de test : [[<https://framagit.org/bib/labase/exp-rimentations/-/tree/master/COVID19/MASKS%20Testings/DIY%20TEST%40FILTRE_Atelier%20COVID-19/05-11et12>][photos]
-   Début de la réalisation du protocole de test -> nous avons conçu le protocole et dans ce même temps les novices/profanes en la matière ont pu se familiariser avec les manipulations en microbiologie.


<a id="orgaa590a2"></a>

### 13 mai

-   Nous continuons les recherches sur les masques DIY et nous bricolons un dispositif de test - Test@Filtre - simplifié.


<a id="org7d98c62"></a>

### 14 mai

-   Mise en place d'un protocole plus clair et plus efficace. Le dispositif de test prêt, fonctionnel.


<a id="org083874d"></a>

### 18 mai

-   Réalisation d'un système pour capter la pression avec un arduino (Erwan surtout)
-   schéma définitif


<a id="orgb7468c9"></a>

### du 19 mai au 29 juillet

-   Réalisation des tests en fonction des différents textiles, évolution du protocole, collecte de données (comptage, cahier de manips, framacalc) et documentation (photos, notes, dokuwiki, framagit). Les textiles testés :
    1.  Essui-tout (5marques)
    2.  Lingettes dépoussiérantes (3 marques)
    3.  Coton (t-shirt et chiffon)
    4.  Wax
    5.  Masques en tissu à 1,2 et 3 couches (collectivités, institutions, entreprises)
    6.  Sac aspirateurs (4 marques)
    7.  Blouses chirurgicales
    8.  Masques chirurgicaux
    9.  Assemblages
-   Documentation des données récoltées dans un carnet de manipulations dédié au labo au BIB et report sur un [calc-partagé](https://lite.framacalc.org/9hiz-testfiltre_resultats).
-   Rassemblement des élements de recherche et résultats sur le cloud du BIB.
-   Nous invitons chacun à venir au BIB, puisque LABASE peut maintenant vous proposer de tester vos masques ou n'importe quelle composition textile.


<a id="org366548a"></a>

## Courant Août

-   C'est calme, on avance doucement, certains sont en vacances.


<a id="org24551b8"></a>

## Courant Septembre

-   Réalisation colective de 2 vidéos tutoriels et explicatives :
    1.  [La première expliquant le montage du DIY Test@Filtre](https://www.youtube.com/watch?v=km8is-onO1w&feature=youtu.be&ab_channel=TanguiD)
    2.  [La seconde faisant la démonstration des différentes manipulations du protocole de test](https://www.youtube.com/watch?v=9T8CL6yV5iU&feature=youtu.be&ab_channel=TanguiD)
-   Travail réalisé à LABASE, au labo, à distance grâce à Jitsi, ainsi qu'au cloud du BIB (espace de stockage et partage) :
    1.  Traitement et analyses des données récoltées lors des tests.
    2.  Représentation au travers de diagrammes.
    3.  Approfondissement et vérifications des données, réalisation de nouveaux tests pour déterminer les conditions de test et des protocoles. Par exemple le débit, la pression, la respirabilité.
    4.  Début de la finalisation de l'écriture de l'article côté microbio/virologie. Première ébauche des idées pour la partie socio-technique de l'article.


<a id="orgcf9ca0e"></a>

## Début octobre

-   Finalisation de la partie microbio et virologie de l'article.
-   Mise en marche de la rédaction de la partie socio-technique, notamment par la comparaison de l'initiative de la quotidienne COVID de LABASE et du DIY Test@Filtre avec les autres initiatives prises (hackaton, fab-lab, makeurs, etc) durant la crise. Puis par l'analyse des notes effectuées durant l'ensemble de l'initiative collective, afin de tenter de définir une grille d'analyse à propos de la recherche d'autonomie.
-   Organisation d'un atelier Test@Filtre dans le cadre de l'université d'automne du BIB (28 octobre – annulées pour cause de deuxième confinement)


<a id="org05296e4"></a>

## Période de reconfinement (30 octobre au &#x2026;)


<a id="orga313ba3"></a>

### mi- et fin-octobre (reconfinement)

-   Relance des tests : pour les données manquantes ainsi que pour préciser les données et résultats
-   Avancée de l'article (microbio et socio-technique) dans l'objectif d'une publication et d'un vaste partage aux différentes communautés, collectifs, amis, compas.

*Comment ça s'est passé, les notes de documentation :*
<https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:notes-covid>
et <https://lebib.org/wiki/doku.php?id=labase:medecine-autonome:covid19>
